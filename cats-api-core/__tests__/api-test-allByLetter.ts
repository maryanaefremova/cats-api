import Client from '../../dev/http-client';

const HttpClient = Client.getInstance();

const limit = 10;
const order = 'asc';
const gender = 'female';

describe('API получения списка котов', () => {
    it('Получить сгруппированный список котов', async () => {
        const url = `core/cats/allByLetter?limit=${limit}&order=${order}&gender=${gender}`;
        const response = await HttpClient.get(url, {
            responseType: 'json',
        });
        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual(expect.objectContaining({
            count_all: expect.any(Number),
            count_output: expect.any(Number),
            groups: expect.arrayContaining([
                expect.objectContaining({
                    title: expect.any(String),
                    cats: expect.arrayContaining([
                        expect.objectContaining({
                            id: expect.any(Number),
                            name: expect.any(String),
                            description: expect.any(String),
                            gender: gender,
                            likes: expect.any(Number),
                            dislikes: expect.any(Number),
                            count_by_letter: expect.any(String)
                        }),
                    ]),
                    count_by_letter: expect.any(Number),
                    count_in_group: expect.any(Number),
                }),
            ]),
        }));
    });
});